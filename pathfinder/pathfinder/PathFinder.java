package pathfinder;

/**
 * Created by kateh on 5/31/2017.
 *finds path from a location on a tile and finds another
 */
public interface PathFinder {
    /**
     * include:
     * @param mover provides info
     * @param startx x coordinate of start point
     * @param starty y coordinate of start point
     * @param tarx x coordinate of target point
     * @param tary y coordinate of target point
     */

    public Path findPath(Mover mover, int startx, int starty, int tarx, int tary);
}
