package pathfinder;
import java.util.ArrayList;
/**
 * Created by Kateh on 6/1/2017.
 * finds the steps in a certain path, from start point to target
 */
public class Path {

    private ArrayList steps = new ArrayList();

    public Path(){

    }

    public int getLength(){
        return steps.size();
    }

    public Step getStep (int index){
        return (Step) steps.get(index);
        /*step is gotten from index w/in a path, must be >= 0 and < getLength*/
    }

    public int getX(int index){
        return getStep(index).x;
        /*X coordinate of the step*/
    }

    public int getY(int index){
        return getStep(index).y;
        /*Y coordinate of the step*/
    }

    /*appends "step" to the path*/
    public void appendStep(int x, int y){
        steps.add(new Step(x,y));
    }

    /*prepend a "step" to the path*/
    public void prependStep(int x, int y){
        steps.add(0, new Step(x,y));
    }

    /*checks path if it contains the given "step"*/
    public boolean contains(int x, int y){
        return steps.contains(new Step(x,y));
    }

    public class Step{
        private int x, y;

        public Step(int x, int y){
            this.x = x;
            this.y = y;
        }

        public int getX(int x){
            return x;
        }

        public int getY(int y){
            return y;
        }
        /**@see Object#hashCode()*/
        public int hashCode(){
            return x*y;
        }

        /**@see Object#equals(Object)*/
        public boolean equals(Object other){
            if (other instanceof Step){
                Step o  = (Step) other;
                return (o.x == x ) && (o.y == y);
            }
            return false;
        }
    }
}
