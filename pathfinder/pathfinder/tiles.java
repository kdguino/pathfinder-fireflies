package pathfinder;

/**
 * Created by kateh on 6/5/2017.
 * connection between data being searched and path finding tools
 */
public interface tiles {

    public int getTileWidth(); /**gets width of tile map, returns no. of tiles across the map*/

    public int getTileHeight(); /**gets height of tile map, returns no. of tiles down the map*/

    public void pathVisited(); /**notifies when a given path is searched/visited already*/

    public boolean blocked(); /** checks if the path is blocked*/

    public float getCost(Mover mover, int startx, int starty, int tarx, int tary ); /** gets the cost of moving through a tile*/





}
