package pathfinder;

import java.util.ArrayList;
import java.util.Collections;
import pathfinder.heuristic;



/**
 * Created by kateh on 6/6/2017.
 * applies the heuristic
 */
public class MainPathFinder implements PathFinder{


    private ArrayList closed = new ArrayList(); /**set that has been searched*/
    private SortedList open = new SortedList(); /**set that has not been fully searched*/

    private tiles tile; /**the tile/map being searched*/

    private int searchDistMax; /**maximum amount of search before giving up*/

    private Node [][] nodes; /**set of nodes across the map*/

    private boolean allowDiagMove; /**allows diagonal movement*/

    private Aheuristic heuristic;

    public MainPathFinder(tiles tile, int searchDistMax, boolean allowDiagMove, Aheuristic heuristic){
        this.heuristic = heuristic;
        this.tile = tile;
        this.searchDistMax = searchDistMax;
        this.allowDiagMove = allowDiagMove;

        nodes = new Node [tile.getTileWidth()] [tile.getTileHeight()];
        for (int x=0;x<tile.getTileWidth();x++){
            for (int y=0;y<tile.getTileHeight();y++){
               nodes [x][y] = new Node(x,y);

            }
        }

    }

    public Path findPath(Mover mover, int startx, int starty, int tarx, int tary) {
        //checks if destination is blocked
        if (tile.blocked()) {
            return null;
        }
        nodes[startx][starty].cost = 0;
        nodes[startx][starty].depth = 0;
        closed.clear();
        open.clear();
        open.add(nodes[startx][starty]);
        nodes[tarx][tary].parent = null;

        int maxDepth = 0;
        while ((maxDepth < searchDistMax) && (open.size() != 0)) {
            //pulls out the first node to open list

            Node current = getFirstOpen();
            if (current == nodes[tarx][tary]) {
                break;
            }
            removefromOpen(current);
            addtoClosed(current); //search through all the neighbors

            for (int x = -1; x < 2; x++) {
                for (int y = -1; y < 2; y++) {
                    //not the neighbor but current tile

                    if ((x == 0) && (y == 0)) {
                        continue; //if not allowing diagonal movement
                    }
                    if (!allowDiagMove) {
                        if ((x != 0) && (y != 0)) {
                            continue;
                        }
                    }//determine neighbor's location
                    int xp = x + current.x;
                    int yp = y + current.y;

                    if (isValidLocation(mover, startx, starty, xp, yp)){
                        //cost is current cost plus movement cost. heuristic is only used in sorted open list
                        float nextStepCost = current.cost + getMoveCost(mover, current.x, current.y, xp, yp);
                        Node neighbor = nodes [xp][yp];
                        tile.pathVisited();

                        if (nextStepCost < neighbor.cost){
                            if (inOpenList(neighbor)){
                                removefromOpen(neighbor);
                            }
                            if (inClosedList(neighbor)){
                                removefromClosed(neighbor);
                            }
                        }

                        if (!inOpenList(neighbor) && (inClosedList(neighbor))){
                            neighbor.cost = nextStepCost;
                            neighbor.heuristic = getHeuristicCost(mover, xp, yp, tarx, tary);
                            maxDepth = Math.max(maxDepth, neighbor.setParent(current));
                            addToOpen(neighbor);
                        }
                    }


                }

            }
        } if(nodes[tarx][tary].parent == null)
            return null;

        Path path = new Path();
        Node target = nodes[tarx][tary];
        while (target != nodes[startx][starty]){
            path.prependStep(target.x, target.y);
            target = target.parent;
        } path.prependStep(startx,starty);
        return path;

    }
    private Node getFirstOpen(){
        return (Node) open.first();
    }

    private void addToOpen (Node node){
        open.add(node);
    }
    private boolean inOpenList (Node node){
        return open.contains(node);
    }
    private void addtoClosed (Node node){
        closed.add(node);
    }
    private boolean inClosedList(Node node){
        return closed.contains(node);
    }
    private void removefromClosed (Node node){
        closed.remove(node);
    }
    private void removefromOpen (Node node){
        open.remove(node);
    }
    private boolean isValidLocation (Mover mover, int startx, int starty, int x, int y){
        boolean invalid = (x < 0 ) || (y < 0) || (x>=tile.getTileWidth()) || (y>=tile.getTileHeight());
        if ((!invalid) && ((startx != x) || (starty != y))){
            invalid = tile.blocked();
        }
        return invalid;

    }
    public float getMoveCost(Mover mover, int startx, int starty, int tarx, int tary){
        return tile.getCost(mover,startx,starty,tarx,tary);
    }
    public float getHeuristicCost(Mover mover, int x, int y, int tarx, int tary){
        return heuristic.getCost(tile, mover, x , y, tarx, tary);
    }

    private class SortedList{
        private ArrayList list = new ArrayList();

        public Object first(){
            return list.get(0);
        }

        public void clear(){
            list.clear();
        }
        public void add(Object o){
            list.add(o);
            Collections.sort(list);
        }
        public void remove(Object o){
            list.remove(o);
        }
        public int size (){
            return list.size();
        }
        public boolean contains(Object o ){
            return list.contains(o);
        }
    }
    private class Node implements Comparable{
        private int x;
        private int y;
        private float cost;
        private Node parent;
        private float heuristic;
        private int depth;

        public Node (int x, int y){
            this.x = x;
            this.y = y;
        }

        public int setParent(Node parent){
            depth = parent.depth + 1;
            this.parent = parent;

            return depth;
        }

        public int compareTo(Object other){
            Node o = (Node) other;

            float f = heuristic + cost;
            float of = o.heuristic + o.cost;

            if (f < of){
                return -1;
            } else if (f > of){
                return 1;
            }else
                return 0;
        }
    }


}
