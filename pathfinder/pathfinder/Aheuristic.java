package pathfinder;

/**
 * Created by kateh on 6/3/2017.
 * just passes values to >heuristic class
 *  */
public interface Aheuristic {

    public float getCost (tiles tile, Mover mover, int tx, int ty, int x, int y);


}
