package pathfinder;


/**
 * Created by kateh on 6/2/2017.
 * gets the cost of each tile
 *
 */
public class heuristic implements Aheuristic {

    public float getCost( tiles tile, Mover mover, int tx, int ty, int x, int y){
        float dx = tx - x;
        float dy = ty - y;

        float result  = (float) (Math.sqrt((dx*dx)+(dy*dy)));

        return result;
    }


}
